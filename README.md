A python-only implmentation of an MTree data structure, which indexes items based on their distance in a metric space.

Closely follows the implementation as described here: https://en.wikipedia.org/wiki/M-tree