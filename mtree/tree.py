## Implmentation of an MTree as described by
## http://www.vldb.org/conf/1997/P426.PDF
## and
## http://www-db.deis.unibo.it/research/papers/SEBD97.pdf
## (which are also copied in the docs folder)

import codecs
import json
import pickle
import random
from typing import Any, Callable, Tuple

## TODO: MTree should handle memoization of dist function, and should output that memoization to the serialized format
## TODO: Move all serialization logic to serialization.py
## TODO: Implement nearest neighbor search?
## TODO: Implement functions to get number of items in the tree


class MTreeValueObject:
    """
    A wrapper object around the feature values submitted into the tree.  Also functions as the base class for 
    `MTreeDataObjects` and `MTreeRoutingObjects`
    """
    def __init__(self, featureValue):
        self.featureValue = featureValue
        self.distanceToParent: float = 0

    def reprJSON(self, valueEncoder):
        return {
            "_type": self.__class__.__name__, 
            "featureValue": {
                "_type": "featureValue",
                "encoded": valueEncoder(self.featureValue)
            },
            "distanceToParent": self.distanceToParent
        }

class MTreeDataObject(MTreeValueObject):
    """
    The Data objects that are stored within `MTreeLeafNodes`
    """
    def __init__(self, value, objectId=None):
        super(MTreeDataObject, self).__init__(value)
        self.objectId = objectId

    def reprJSON(self, valueEncoder):
        parent = super().reprJSON(valueEncoder)
        parent.update({"objectId": self.objectId})
        return parent

class MTreeNode:
    """
    Base class for MTree nodes containing shared attributes and methods
    """
    def __init__(self, tree: 'MTree', parent: 'MTreeRoutingObject'):
        self.tree: MTree = tree
        self.parentRouter: MTreeRoutingObject = parent
        self.entries: list[MTreeValueObject] = None
        # self.dist_func: Callable[[MTreeValueObject, MTreeValueObject], float] = tree.dist_func ## distance_func

    def range(self, item: MTreeValueObject, searchRadius) -> list[MTreeDataObject]:
        raise NotImplementedError("abstract")

    def insert(self, item):
        raise NotImplementedError("abstract")

    def addEntries(self, entries: list[MTreeValueObject]):
        raise NotImplementedError("abstract")

    def clearItems(self):
        raise NotImplementedError("abstract")

    def split(self, newEntry:MTreeDataObject):
        # info = "(root)"
        # if self.parentRouter is not None:
        #     info = f"{self.parentRouter.featureValue} ({self.parentRouter.coveringRadius})"
        # print(f"Splitting {info}: {[o.featureValue for o in self.entries]} with {newEntry.featureValue}:")
        oldParentRouter = self.parentRouter

        o1, o2, set_o1, set_o2 = self.tree.split_policy(self, newEntry)

        ## This node will take on the values in set_ro1...
        self.clearItems()
        routingObject1 = MTreeRoutingObject(o1.featureValue, self)
        self.parentRouter = routingObject1
        self.addEntries(set_o1)

        ## and the new node will take on the values in set_ro2...
        newNode = MTreeLeafNode(self.tree, None)
        routingObject2 = MTreeRoutingObject(o2.featureValue, newNode)
        newNode.parentRouter = routingObject2
        newNode.addEntries(set_o2)

        # print(f"  {routingObject1.featureValue} ({routingObject1.coveringRadius}): {[o.featureValue for o in set_o1]}")
        # print(f"  {routingObject2.featureValue} ({routingObject2.coveringRadius}): {[o.featureValue for o in set_o2]}")

        if self.tree.root == self:
            ## If this is the root node, then create a new root node above us
            ## and store the new routing objects in it
            newRoot = MTreeInnerNode(self.tree, None)
            newRoot.addEntries([routingObject1,routingObject2])
            self.tree.root = newRoot
        else:
            oldParentNode = oldParentRouter.parentNode
            oldParentNode.removeEntry(oldParentRouter)
            oldParentNode.addEntries([routingObject1])
            if self.parentRouter.parentNode.isFull():
                self.parentRouter.parentNode.split(routingObject2)
            else:
                self.parentRouter.parentNode.addEntries([routingObject2])

class MTreeLeafNode(MTreeNode):
    def __init__(self, tree: 'MTree', parent: 'MTreeRoutingObject'):
        super(MTreeLeafNode, self).__init__(tree, parent)
        self.entries: list[MTreeDataObject] = []
        self.maxEntries = 10
    
    def reprJSON(self, valueEncoder):
        return {"_type": self.__class__.__name__, "entries": self.entries, "maxEntries": self.maxEntries}

    def range(self, item: MTreeValueObject, searchRadius) -> list[MTreeDataObject]:
        results: list[MTreeDataObject] = []
        for dataObject in self.entries:
            distItemToOurParent = self.tree.dist_func(item, self.parentRouter) if self.parentRouter is not None else 0

            if abs(distItemToOurParent - dataObject.distanceToParent) <= searchRadius:
                if self.tree.dist_func(item, dataObject) <= searchRadius:
                    results.append(dataObject)
        return results

    def insert(self, item: MTreeValueObject):
        ## If we were passed a data object, then use it directly.
        ## Otherwise, create a new DataObject object to wrap it
        entry: MTreeDataObject = None
        if isinstance(item, MTreeDataObject):
            entry = item
        else:
            entry = MTreeDataObject(item.featureValue)

        ## Add this item to this leaf if there is room
        if len(self.entries) < self.maxEntries:
            self.addEntries([entry])
        else:
            self.split(entry)

    def clearItems(self):
        self.entries = []
    
    def addEntries(self, items: list[MTreeDataObject]):
        for item in items:
            if (self.parentRouter is not None):
                item.distanceToParent = self.tree.dist_func(item, self.parentRouter)
            self.entries.append(item)
        
        if self.parentRouter is not None:
            self.parentRouter.expandRadius(items, self.tree.dist_func)

class MTreeInnerNode(MTreeNode):
    def __init__(self, tree: 'MTree', parent):
        super(MTreeInnerNode, self).__init__(tree, parent)
        self.entries: list[MTreeRoutingObject] = []  ## MTreeRoutingObject
        self.maxEntries = 10
    
    def reprJSON(self, valueEncoder):
        return {"_type": self.__class__.__name__, "entries": self.entries, "maxEntries": self.maxEntries}

    def range(self, item: MTreeValueObject, searchRadius: float) -> list[MTreeDataObject]:
        results: list[MTreeDataObject] = []
        for routingObject in self.entries:
            distItemToOurParent = self.tree.dist_func(item, self.parentRouter) if self.parentRouter is not None else 0

            if abs(distItemToOurParent - routingObject.distanceToParent) <= routingObject.coveringRadius + searchRadius:
                if self.tree.dist_func(item, routingObject) <= routingObject.coveringRadius + searchRadius:
                    results += routingObject.coveringTree.range(item, searchRadius)
        return results
    
    def insert(self, item: MTreeValueObject):
        routeDistances = [(ro, self.tree.dist_func(item, ro)) for ro in self.entries]
        routeDistances.sort(key=lambda t: t[1] - t[0].coveringRadius) 
        closestRoute, closestDistance = routeDistances[0]

        ## If the distance to the closest route is still larger than the covering radius
        ## of that routing object, then we must expand the radius of that routing object
        if closestDistance > closestRoute.coveringRadius:
            closestRoute.coveringRadius = closestDistance

        closestRoute.coveringTree.insert(item)

    def clearItems(self):
        self.entries = []

    def addEntries(self, items: list['MTreeRoutingObject']):
        for item in items:
            item.parentNode = self
            if (self.parentRouter is not None):
                item.distanceToParent = self.tree.dist_func(item, self.parentRouter)
            self.entries.append(item)
        
        if self.parentRouter is not None:
            self.parentRouter.expandRadius(items, self.tree.dist_func)

    def removeEntry(self, entryToRemove: 'MTreeRoutingObject'):
        self.entries.remove(entryToRemove)

    def isFull(self):
        return len(self.entries) >= self.maxEntries

class MTreeRoutingObject(MTreeValueObject):
    def __init__(self, featureValue, coveringTree: MTreeNode):
        super(MTreeRoutingObject, self).__init__(featureValue)
        self.coveringTree: MTreeNode = coveringTree
        self.coveringRadius: float = 0
        self.parentNode: MTreeInnerNode = None

    def reprJSON(self, valueEncoder):
        sup = super().reprJSON(valueEncoder)
        sup.update({"coveringTree": self.coveringTree, "coveringRadius": self.coveringRadius})
        return sup

    def expandRadius(self, items: list[MTreeValueObject], dist_func):
        for item in items:
            distToItem = dist_func(item, self) + getattr(item, "coveringRadius", 0)
            if distToItem > self.coveringRadius:
                self.coveringRadius = distToItem

def promote_RANDOM(node: MTreeNode, newEntry: MTreeValueObject) -> Tuple[MTreeValueObject, MTreeValueObject]:
    ## Promote - determine new routing objects randomly
    return random.sample([*node.entries, newEntry], 2)

def partition_GH(node: MTreeNode, newEntry: MTreeValueObject, o1: MTreeValueObject, o2: MTreeValueObject):
    ## Partition - Generalized Hyperplane (sort into closest routing object)
    set_o1: list[MTreeValueObject] = []
    set_o2: list[MTreeValueObject] = []
    for obj in [*node.entries, newEntry]:
        distToO1 = node.tree.dist_func(obj, o1)
        distToO2 = node.tree.dist_func(obj, o2)

        if distToO1 <= distToO2:
            set_o1.append(obj)
        else:
            set_o2.append(obj)
    
    return set_o1, set_o2

def split_policy_RANDOM_GH(node: MTreeNode, newEntry: MTreeValueObject)    :
    o1, o2 = promote_RANDOM(node, newEntry)
    set_o1, set_o2 = partition_GH(node, newEntry, o1, o2)
    
    return o1, o2, set_o1, set_o2

def dist_int(a: MTreeValueObject, b: MTreeValueObject):
    """
    Returns the distance between two integer values
    """
    return abs(a.featureValue-b.featureValue)

class MTree:
    """
    MTree indexes items based on their distance in a metric space.  
    
    `dist_func` takes two `MTreeValueObjects` and returns a distance in the metric space.  This is not memoized.
    
    `split_policy` takes an `MTreeNode` to split and a `MTreeValueObject` to add, and returns the value objects
    to be promoted as routing objects and the lists of the objects partitioned in each.  By default, this uses
    a random promotion policy and generalized hyperplane partition policy.

    `value_encoder` is a function to use to convert value objects into a JSON-serializable format.

    `value_decoder` is a function that converts a JSON object into a feature value; This is the opposite of `value_encoder`.
    """
    def __init__(self, 
            dist_func: Callable[[MTreeValueObject, MTreeValueObject], float], 
            split_policy: Callable[[MTreeNode, MTreeValueObject], Tuple[MTreeValueObject, MTreeValueObject, list[MTreeValueObject], list[MTreeValueObject]]] = split_policy_RANDOM_GH,
            value_encoder: Callable[[Any], Any] = None,
            value_decoder: Callable[[Any], Any] = None):
        self.dist_func: Callable[[MTreeValueObject, MTreeValueObject], float] = dist_func ## distance_func
        self.split_policy: Callable[[MTreeNode, MTreeValueObject], Tuple[MTreeValueObject, MTreeValueObject, list[MTreeValueObject], list[MTreeValueObject]]] = split_policy
        self.root: MTreeNode = MTreeLeafNode(self, None)
        self.value_encoder = value_encoder
        self.value_decoder = value_decoder
    
    def reprJSON(self, valueEncoder):
        return {
            "_type": self.__class__.__name__, 
            "root": self.root, 
        }
    
    def insert(self, item):
        if not isinstance(item, MTreeValueObject):
            item = MTreeValueObject(item)
        self.root.insert(item)

    def range(self, item, searchRadius):
        if not isinstance(item, MTreeValueObject):
            item = MTreeValueObject(item)
        return self.root.range(item, searchRadius)

    def visitNodes(self, node: MTreeNode = None, operation: Callable[[MTreeNode, Any], None] = None, **kwargs):
        if node is None:
            node = self.root

        ## Perform the operation on this node
        operation(node, **kwargs)

        ## Visit descendent nodes if any
        if isinstance(node, MTreeInnerNode):
            for e in node.entries:
                self.visitNodes(e.coveringTree, operation, **kwargs)

    ## TODO implement nearest neighbor search?
