from .context import mtree

def test_int():
    t = mtree.MTree(mtree.dist_int)

    t.insert(4)
    t.insert(6)
    t.insert(9)
    t.insert(1)
    t.insert(12)
    t.insert(13)
    t.insert(14)
    t.insert(15)
    t.insert(16)
    t.insert(17) # 10
    t.insert(18) # 11
    for x in range(19, 19+10):
        t.insert(x)   ## insert 19-29, total of 21 values


    results = t.range(6, 10)

    print(sorted([r.featureValue for r in results]))
    print([r.featureValue for r in results])

def test_strings_len():
    def distance_strlen(a: mtree.MTreeValueObject, b: tree.MTreeValueObject):
        return abs(len(a.featureValue)-len(b.featureValue))

    t = mtree.MTree(distance_strlen)

    t.insert("a")
    t.insert("or")
    t.insert("tom")
    t.insert("pore")
    t.insert("holes")
    t.insert("strand")
    t.insert("breaker")

    results = t.range("break", 2)

    print(sorted([r.featureValue for r in results]))
    print([r.featureValue for r in results])



if __name__ == "__main__":
    test_int()