from asyncio import InvalidStateError
import json
from typing import Any

import mtree

def visitOperationSetTree(node: mtree.MTreeNode, tree=None):
    node.tree = tree


class MTreeJSONDecoder(json.JSONDecoder):
    def __init__(self, *args, featureValueDecoder=None, dist_func=None, split_policy=None, **kwargs):
        json.JSONDecoder.__init__(self, object_hook=self.object_hook, *args, **kwargs)
        self.featureValueDecoder = featureValueDecoder
        self.dist_func = dist_func
        self.split_policy = split_policy

    def object_hook(self, d):
        typ = d["_type"]
        if typ == "featureValue":
            return self.featureValueDecoder(d["encoded"])
        if typ == "MTreeDataObject":
            obj = mtree.MTreeDataObject(d["featureValue"])
            obj.distanceToParent = d["distanceToParent"]
            obj.objectId = d["objectId"]
            return obj
        if typ == "MTreeLeafNode":
            obj = mtree.MTreeLeafNode(None, None)
            obj.entries = d["entries"]
            obj.maxEntries = d["maxEntries"]
            return obj
        if typ == "MTreeRoutingObject":
            obj = mtree.MTreeRoutingObject(d["featureValue"], None)
            obj.coveringTree = d["coveringTree"]
            obj.coveringRadius = d["coveringRadius"]
            obj.distanceToParent = d["distanceToParent"]
            ## Need to set parentRouter field for coveringTree root
            obj.coveringTree.parentRouter = obj
            return obj
        if typ == "MTreeInnerNode":
            obj = mtree.MTreeInnerNode(None, None)
            obj.entries = d["entries"]
            obj.maxEntries = d["maxEntries"]
            ## Set the parentNode field for all entries below
            for e in obj.entries:
                e.parentNode = obj
            return obj
        if typ == "MTree":
            obj = mtree.MTree(None)
            obj.dist_func = self.dist_func
            obj.split_policy = self.split_policy
            obj.value_encoder = None
            obj.value_decoder = None
            obj.root = d["root"]

            ## Fill in the tree field for all nodes
            obj.visitNodes(operation=visitOperationSetTree, tree=obj)
            return obj
        else:
            return d

class MTreeJSONEncoder(json.JSONEncoder):
    def __init__(self, *args, featureValueEncoder=None, **kwargs):
        json.JSONEncoder.__init__(self, *args, **kwargs)
        self.featureValueEncoder = featureValueEncoder

    def default(self, obj):
        if hasattr(obj,'reprJSON'):
            return obj.reprJSON(self.featureValueEncoder)
        else:
            return json.JSONEncoder.default(self, obj)

def simpleValueEncoder(featureValue: Any) -> str:
    """
    Prepares a feature value for encoding by simply returning itself.
    This would be a valid encoder for featureValues that are already
    natively representable in JSON, such as strings or numbers.
    """
    return featureValue

def simpleValueDecoder(encodedJsonValue: Any) -> Any:
    """
    This decodes a feature value by simply returning the decoded
    JSON value.
    """
    return encodedJsonValue

## These examples demonstrate a silly way to encode and decode integer values
def complicatedValueEncoder(featureValue: int) -> str:
    return f"complicated:{featureValue}"

def complicatedValueDecoder(encodedJsonValue: str) -> int:
    return int(encodedJsonValue.split(":", 1)[1])

## The actual serialization and deserialization methods

def DeserializeMTree(serializedJson: str, dist_func=None, split_policy=None, featureValueDecoder=simpleValueDecoder) -> mtree.MTree:
    """
    Deserializes the given JSON string into an MTree.

    `featureValueEncoder` is a function that takes natively deserialized JSON object (e.g. a `dict`, `str`, or other structure) and returns a proper feature value.

    `dist_func` and `split_policy` methods should be the same as provided to the original tree.
    """
    if dist_func is None or split_policy is None:
        raise InvalidStateError("A tree cannot be deserialized without providing a distance function or split policy")
    
    return json.loads(serializedJson, cls=MTreeJSONDecoder, featureValueDecoder=featureValueDecoder, dist_func=dist_func, split_policy=split_policy)

def SerializeMTree(tree: mtree.MTree, featureValueEncoder=simpleValueEncoder) -> str:
    """
    Serializes the given MTree into a JSON string.  

    `featureValueEncoder` is a function that takes a feature value and returns an object that can be natively serialized, e.g. a `dict`, `str`, or other structure.  

    Note that the `dist_func` and `split_policy` methods cannot be serialized.  When deserializing this tree, those methods must be provided again.
    """
    return json.dumps(tree, cls=MTreeJSONEncoder, indent="  ", featureValueEncoder=featureValueEncoder)


if __name__ == "__main__":
    ## Some simple test code to create a tree, serialize/deserialize it, and add more values to it to induce a node split
    t = mtree.MTree(lambda x,y: abs(x.featureValue-y.featureValue))
    t.insert(5)
    t.insert(6)
    t.insert(9)

    j = SerializeMTree(t)

    print(j)

    ts = DeserializeMTree(j, dist_func=t.dist_func, split_policy=t.split_policy)

    for x in range(10,20):
        ts.insert(x)

    js = SerializeMTree(ts)

    print(js)